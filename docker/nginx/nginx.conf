user www-data;
worker_processes auto;
worker_cpu_affinity auto;

events {
        worker_connections 2048;
        multi_accept on;
}

http {

    ### Basic Settings ###
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    # server_tokens off;

    server_names_hash_bucket_size 64;
    # server_name_in_redirect off;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;


    ### Logging Settings ###
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    ### Gzip Settings ###
    gzip on;
    gzip_disable "msie6";

    ### Include Config ###
    server {
        listen 80;
        listen [::]:80;

        root /var/www/public;
        index index.php index.html;

        server_name localhost;

        location = /favicon.ico {
            log_not_found off;
            access_log off;
        }

        location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
        }

        location / {
            try_files $uri $uri/ /index.php?$args;
        }

        location ~ \.php$ {
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param PATH_INFO $fastcgi_path_info;
            
            fastcgi_pass php-fpm:9000;
            fastcgi_index index.php;
        }

        location ~ /\.ht {
            deny all;
        }
    }
}